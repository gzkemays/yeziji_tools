package cn.yeziji.entity.args;

import lombok.Builder;
import lombok.Data;

/**
 * Minio 合并参数
 *
 * @author gzkemays
 * @date 2021/9/16 21:07
 */
@Data
@Builder
public class MinioComposeArgs {
  /** 合并的目标桶 */
  String composeBucket;
  /** 合并至某个文件夹 */
  String composeFolder;
  /** 合并的目标文件名 */
  String composeFileName;
  /** 分片所在的桶 */
  String chuckBucket;
  /** 分片所在的文件夹 */
  String chuckFolder;
  /** 分片的统一前缀 */
  String chuckPrefix;
  /** 合并目标文件的 contentType */
  String contentType;
  /** 是否采取编码，默认为 true */
  boolean useDecode;
  /** 是否删除合并分片 */
  boolean deleteChuck;
}
