package cn.yeziji.entity.args;

import lombok.Builder;
import lombok.Data;

/**
 * Minio 常用单体属性
 *
 * @author gzkemays
 * @date 2021/9/17 15:20
 */
@Data
@Builder
public class MinioEntityArgs {
  /** 桶的位置 */
  String bucket;
  /** 文件名位置 */
  String object;
  /** 所在文件夹 */
  String folder;
  /** 查询前缀 */
  String prefix;
  /** 是否开启递归 */
  boolean recursive;
}
