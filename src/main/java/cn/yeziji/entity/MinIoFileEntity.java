package cn.yeziji.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author gzkemays
 * @date 2021/4/17 23:02
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MinIoFileEntity {
  String bucketName;
  String fileName;
  String updateTime;
  String fileSize;
}
