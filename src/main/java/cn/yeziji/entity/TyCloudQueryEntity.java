package cn.yeziji.entity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 * @author gzkemays
 * @date 2021/2/24 10:28
 */
public class TyCloudQueryEntity {
  public static final String ACCOUNT_ID = "account_id";
  public static final String CREATE_TIME = "create_time";
  public static final String SUBMIT_TIME = "submit_time";
  public static final String SUBMIT_ID = "submit_id";
  public static final String URL = "url";
  public static final String STATUS = "status";
  public static final String ID = "id";

  private String account_id;
  private Date create_time;
  private Date submit_time;
  private String submit_id;
  private String id;
  private String url;
  private int status;

  interface builder {
    TyCloudQueryEntity build();
  }

  private TyCloudQueryEntity() {}

  public static TyCloudQueryEntityBuilder builder() {
    return new TyCloudQueryEntityBuilder();
  }

  public static final class TyCloudQueryEntityBuilder implements builder {
    public static final ThreadLocal<SimpleDateFormat> SIMPLE_DATE_FORMAT_THREAD_LOCAL =
        new ThreadLocal<>();
    private static final String FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String COMPLETED = "completed";
    private String account_id;
    private Date create_time;
    private Date submit_time;
    private String submit_id;
    private String id;
    private String url;
    private int status;

    private TyCloudQueryEntityBuilder() {}

    public static TyCloudQueryEntityBuilder TyCloudQueryEntity() {
      return new TyCloudQueryEntityBuilder();
    }

    public TyCloudQueryEntityBuilder accountId(String account_id) {
      this.account_id = account_id;
      return this;
    }

    public TyCloudQueryEntityBuilder createTime(String create_time) {
      this.create_time = CONVERT_TIME(create_time);
      return this;
    }

    public TyCloudQueryEntityBuilder submitTime(String submit_time) {
      this.submit_time = CONVERT_TIME(submit_time);
      return this;
    }

    public TyCloudQueryEntityBuilder submitId(String submit_id) {
      this.submit_id = submit_id;
      return this;
    }

    public TyCloudQueryEntityBuilder id(String id) {
      this.id = id;
      return this;
    }

    public TyCloudQueryEntityBuilder url(String url) {
      this.url = url;
      return this;
    }

    public TyCloudQueryEntityBuilder status(String status) {
      this.status = status.equals(COMPLETED) ? 1 : 0;
      return this;
    }

    @Override
    public TyCloudQueryEntity build() {
      TyCloudQueryEntity tyCloudQueryEntity = new TyCloudQueryEntity();
      tyCloudQueryEntity.account_id = this.account_id;
      tyCloudQueryEntity.create_time = this.create_time;
      tyCloudQueryEntity.url = this.url;
      tyCloudQueryEntity.submit_id = this.submit_id;
      tyCloudQueryEntity.id = this.id;
      tyCloudQueryEntity.status = this.status;
      tyCloudQueryEntity.submit_time = this.submit_time;
      return tyCloudQueryEntity;
    }

    private Date CONVERT_TIME(String date) {
      SimpleDateFormat sdf;
      if (Objects.isNull(SIMPLE_DATE_FORMAT_THREAD_LOCAL.get()))
        SIMPLE_DATE_FORMAT_THREAD_LOCAL.set(new SimpleDateFormat(FORMAT));
      sdf = SIMPLE_DATE_FORMAT_THREAD_LOCAL.get();
      try {
        return sdf.parse(date);
      } catch (Exception e) {
        e.printStackTrace();
      }
      return null;
    }
  }

  public String getAccount_id() {
    return account_id;
  }

  public void setAccount_id(String account_id) {
    this.account_id = account_id;
  }

  public Date getCreate_time() {
    return create_time;
  }

  public void setCreate_time(Date create_time) {
    this.create_time = create_time;
  }

  public Date getSubmit_time() {
    return submit_time;
  }

  public void setSubmit_time(Date submit_time) {
    this.submit_time = submit_time;
  }

  public String getSubmit_id() {
    return submit_id;
  }

  public void setSubmit_id(String submit_id) {
    this.submit_id = submit_id;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  @Override
  public String toString() {
    return "TyCloudQueryEntity{"
        + "account_id='"
        + account_id
        + '\''
        + ", create_time="
        + create_time
        + ", submit_time="
        + submit_time
        + ", submit_id='"
        + submit_id
        + '\''
        + ", id='"
        + id
        + '\''
        + ", url='"
        + url
        + '\''
        + ", status="
        + status
        + '}';
  }
}
