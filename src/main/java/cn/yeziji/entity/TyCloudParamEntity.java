package cn.yeziji.entity;

import java.util.List;

/**
 * @author gzkemays
 * @date 2021/2/4 15:41
 */
public class TyCloudParamEntity {
  private String account_id;
  private List<String> values;
  private String url;
  private String submit_id;
  private String task_id;
  private Integer type = 0;
  private Long start_time;
  private Long end_time;
  private Integer page;
  private Integer page_size;

  interface builder {
    TyCloudParamEntity getters();
  }

  public static TyCloudParamBuilder options() {
    return new TyCloudParamBuilder();
  }

  public TyCloudParamEntity(TyCloudParamBuilder builder) {
    this.account_id = builder.account_id;
    this.values = builder.values;
    this.url = builder.url;
    this.submit_id = builder.submit_id;
    this.task_id = builder.task_id;
    this.type = builder.type;
    this.start_time = builder.start_time;
    this.end_time = builder.end_time;
    this.page = builder.page;
    this.page_size = builder.page_size;
  }

  public static class TyCloudParamBuilder implements builder {
    private String account_id = "ee821339cc054bcd819e3ee84fbd6e34";
    private List<String> values;
    private String url;
    private String submit_id;
    private String task_id;
    private Integer type;
    private Long start_time;
    private Long end_time;
    private Integer page;
    private Integer page_size;

    @Override
    public TyCloudParamEntity getters() {
      return new TyCloudParamEntity(this);
    }

    public TyCloudParamBuilder accountId(String accountId) {
      this.account_id = accountId;
      return this;
    }

    public TyCloudParamBuilder values(List<String> values) {
      this.values = values;
      return this;
    }

    public TyCloudParamBuilder url(String url) {
      this.url = url;
      return this;
    }

    public TyCloudParamBuilder submitId(String submitId) {
      this.submit_id = submitId;
      return this;
    }

    public TyCloudParamBuilder taskId(String taskId) {
      this.task_id = taskId;
      return this;
    }

    public TyCloudParamBuilder type(Integer type) {
      this.type = type;
      return this;
    }

    public TyCloudParamBuilder startTime(Long startTime) {
      this.start_time = startTime;
      return this;
    }

    public TyCloudParamBuilder endTime(Long endTime) {
      this.end_time = endTime;
      return this;
    }

    public TyCloudParamBuilder page(Integer page) {
      this.page = page;
      return this;
    }

    public TyCloudParamBuilder pageSize(Integer pageSize) {
      this.page_size = pageSize;
      return this;
    }
  }

  public String getAccountId() {
    return account_id;
  }

  public void setAccountId(String account_id) {
    this.account_id = account_id;
  }

  public List<String> getValues() {
    return values;
  }

  public void setValues(List<String> values) {
    this.values = values;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getSubmitId() {
    return submit_id;
  }

  public void setSubmitId(String submit_id) {
    this.submit_id = submit_id;
  }

  public String getTaskId() {
    return task_id;
  }

  public void setTaskId(String task_id) {
    this.task_id = task_id;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public Long getStartTime() {
    return start_time;
  }

  public void setStartTime(Long start_time) {
    this.start_time = start_time;
  }

  public Long getEndTime() {
    return end_time;
  }

  public void setEndTime(Long end_time) {
    this.end_time = end_time;
  }

  public Integer getPage() {
    return page;
  }

  public void setPage(Integer page) {
    this.page = page;
  }

  public Integer getPageSize() {
    return page_size;
  }

  public void setPageSize(Integer page_size) {
    this.page_size = page_size;
  }

  @Override
  public String toString() {
    return "TyCloudParamEntity{"
        + "accountId='"
        + account_id
        + '\''
        + ", values="
        + values
        + ", url='"
        + url
        + '\''
        + ", submitId='"
        + submit_id
        + '\''
        + ", taskId='"
        + task_id
        + '\''
        + ", type="
        + type
        + ", startTime="
        + start_time
        + ", endTime="
        + end_time
        + ", page="
        + page
        + ", pageSize="
        + page_size
        + '}';
  }
}
