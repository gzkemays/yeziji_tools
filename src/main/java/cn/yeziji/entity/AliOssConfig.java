package cn.yeziji.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.InputStream;

/**
 * @author gzkemays
 * @date 2021/2/9 22:46
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AliOssConfig {
  String endPoint;
  String accessKey;
  String secretKey;
  String bucketName;
  String fileName;
  String savePath;
  String ext;
  InputStream stream;
}
