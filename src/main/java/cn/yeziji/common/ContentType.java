package cn.yeziji.common;

/**
 * @author gzkemays
 * @date 2021/2/19 15:15
 */
public class ContentType {
  /** 表单 */
  public static final String FORM_URLENCODED = "application/x-www-form-urlencoded";
  /** 文件上传 */
  public static final String MULTIPART_FORM_DATA = "multipart/form-data";
  /** json 格式 */
  public static final String APPLICATION_JSON = "application/json";
  /** 未知文件类型的 二进制流 */
  public static final String OCTET_STREAM = "application/octet-stream";
  /** text/plain编码 */
  public static final String TEXT_PLAIN = "text/plain";
  /** Rest请求text/xml编码 */
  public static final String TEXT_XML = "text/xml";
  /** text/html编码 */
  public static final String TEXT_HTML = "text/html";
  /** 图片 img/png 编码 */
  public static final String IMG_PNG = "image/png";
  /** 图片 img/jpeg 编码 */
  public static final String IMG_JPEG = "image/jpeg ";
  /** 图片 img/gif 编码 */
  public static final String IMG_GIF = "image/gif";
}
