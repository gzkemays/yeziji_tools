package cn.yeziji.common;

public class FileType {
  public static final String PERIOD_MP3 = ".mp3";
  public static final String MP3 = "mp3";
  public static final String PERIOD_MP4 = ".mp4";
  public static final String MP4 = "mp4";
  public static final String PERIOD_JPEG = ".jpeg";
  public static final String JPEG = "jpeg";
  public static final String PERIOD_PNG = ".png";
  public static final String PNG = "png";
  public static final String PERIOD_GIF = ".gif";
  public static final String GIF = "gif";
  public static final String PERIOD_AVI = ".avi";
  public static final String AVI = "avi";
  public static final String PERIOD_JPG = ".jpg";
  public static final String JPG = "jpg";
}
