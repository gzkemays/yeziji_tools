package cn.yeziji.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum GCommonEnums {
  SUCCESS(200, "请求成功"),
  FAIL(400, "请求异常"),
  HTTP_REQ_NOT_SUPPORTED(400, "请求类型错误"),
  DATA_ERROR(401, "请求数据异常"),
  REQ_ERROR_SUPPORT(402, "HTTP请求异常"),
  SQL_ERROR(500, "数据库异常"),
  SQL_OPERA_ERROR(501, "数据库操作异常"),
  UNKNOWN_ERROR(600, "未知异常"),
  SECURITY_PWD_ERROR(700, "security加密异常");
  // 状态码
  private Integer code;
  // 返回信息
  private String msg;
}
