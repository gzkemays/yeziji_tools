package cn.yeziji.http.tools;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author gzkemays
 * @date 2020/10/29 19:51
 */
public class MParse {
  /**
   * 获取 JSON 格式的数据对象
   *
   * @param jsonStr
   * @param spiltStr
   * @return
   */
  public static String getColData(String jsonStr, String spiltStr) {
    JSONObject jsonObject = JSON.parseObject(jsonStr);
    while (spiltStr.contains(".")) {
      int index = spiltStr.indexOf(".");
      String data = spiltStr.substring(0, index);
      spiltStr = spiltStr.substring(index).replaceFirst(".", "");
      jsonObject = (JSONObject) jsonObject.get(data);
    }

    return jsonObject.getString(spiltStr);
  }

  /**
   * 截取重定向的数据
   *
   * @param uri(a=b&c=d&e=f)
   * @return {a:b,c:d,e:f}
   */
  public static Map<Object, Object> getQueryParam(String uri) {
    Map<Object, Object> map = new HashMap<>();
    String[] strArr;
    strArr = uri.substring(uri.indexOf("&")).replaceFirst("&", "").split("&");
    for (String str : strArr) {
      String[] tempArr;
      tempArr = str.split("=");
      if (tempArr.length > 1) {
        map.put(tempArr[0], tempArr[1]);
      }
    }
    return map;
  }

  /**
   * 截取 Url Query
   *
   * @param uri
   * @return
   */
  public static Map<Object, Object> getQueryURI(String uri) {
    Map<Object, Object> map = new HashMap<>();
    String[] strArr;
    strArr = uri.substring(uri.indexOf("?") + 1).split("&");
    for (String str : strArr) {
      String[] tempArr;
      tempArr = str.split("=");
      if (tempArr.length > 1) {
        map.put(tempArr[0], tempArr[1]);
      }
    }
    return map;
  }
  /** 对象转换成 Map */
  public static Map<Object, Object> getMapFromObject(Object object) throws IllegalAccessException {
    Map<Object, Object> map = new HashMap<>();
    Class<?> clazz = object.getClass();
    for (Field field : clazz.getDeclaredFields()) {
      field.setAccessible(true);
      String key = field.getName();
      Object value = field.get(object);
      map.put(key, value);
    }
    return map;
  }

  /**
   * map 转对象
   *
   * @param target
   * @param source
   * @return
   * @throws IllegalAccessException
   */
  public static Object getObjectFromMap(Object target, Map<Object, Object> source)
      throws IllegalAccessException {
    Class<?> clazz = target.getClass();
    for (Field field : clazz.getDeclaredFields()) {
      field.setAccessible(true);
      field.set(target, source.get(field.getName()));
    }
    return target;
  }

  /**
   * 将 json 写入 resp 流中
   *
   * @param request
   * @param response
   * @param data
   */
  public static void writeJson(
      HttpServletRequest request, HttpServletResponse response, Object data) {
    // 这里很重要，否则页面获取不到正常的JSON数据集
    response.setContentType("application/json;charset=UTF-8");
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.setHeader("Access-Control-Allow-Method", "*");
    // 输出JSON
    PrintWriter out = null;
    try {
      out = response.getWriter();
      out.write(new ObjectMapper().writeValueAsString(data));
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (Objects.nonNull(out)) {
        out.flush();
        out.close();
      }
    }
  }
}
