package cn.yeziji.excel;

import cn.yeziji.excel.annotation.ImportIndex;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.List;

/**
 * excel 转换
 *
 * @author gzkemays
 * @date 2021/8/9 16:18
 */
public class ExcelParse {
  /**
   * 将 excel 转换为 list 对象
   *
   * @param excel 导入的 excel 文件
   * @param clazz 映射的类
   * @return {@link List} 对象数据集
   */
  public static List<Object> parseExcelToList(File excel, Class<?> clazz) {
    if (excel.exists()) {
      try (InputStream is = new FileInputStream(excel)) {
        Workbook workbook = WorkbookFactory.create(is);
        // 获取第一个工作表
        Sheet sheetAt = workbook.getSheetAt(0);
        String[] values;
        // 获取当前工作表的数据，放入 values 的字符串数组当中
        if (sheetAt != null) {
          int i = 2;
          // 第一行都是标题
          Row row = sheetAt.getRow(i);
          if (row != null) {
            int cells = row.getPhysicalNumberOfCells();
            values = new String[cells];
            for (int j = 0; j < cells; j++) {
              // 循环遍历单元格
              Cell cell = row.getCell(j);
              if (cell != null) {
                CellType cellType = cell.getCellType();
                String value = cell.getStringCellValue() == null ? null : cell.getStringCellValue();
                values[j] = value;
              }
            }
          }
        }
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
          // 确定具备导入的索引
          if (field.isAnnotationPresent(ImportIndex.class)) {}
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return null;
  }
}
