package cn.yeziji.excel.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 开启导出的字段
 *
 * <p>可以设置导出的字段宽度
 *
 * @author gzkemays
 * @date 2021/8/7
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EnableExportField {
  /** 导出的宽度 */
  int width() default 100;

  /** 导出的 th 字段名 */
  String name();
}
