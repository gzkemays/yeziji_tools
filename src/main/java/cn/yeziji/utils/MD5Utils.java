package cn.yeziji.utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author gzkemays
 * @date 2021/1/25 14:32
 */
public class MD5Utils {
  /** 首先初始化一个字符数组，用来存放每个16进制字符 */
  private static final char[] HEX_DIGITS = {
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
  };
  /** MD5 加码,生成 32 位md码 */
  public static String stringMd5(String inStr) {
    MessageDigest md5;
    try {
      md5 = MessageDigest.getInstance("MD5");
    } catch (NoSuchAlgorithmException e) {
      return "";
    }
    char[] charArray = inStr.toCharArray();
    byte[] bytesArray = new byte[charArray.length];
    for (int i = 0; i < charArray.length; i++) {
      bytesArray[i] = (byte) charArray[i];
    }
    byte[] md5Bytes = md5.digest(bytesArray);
    StringBuilder hexValue = new StringBuilder();
    for (byte md5Byte : md5Bytes) {
      int val = ((int) md5Byte) & 0xff;
      if (val < 16) {
        hexValue.append("0");
      }
      hexValue.append(Integer.toHexString(val));
    }
    return hexValue.toString();
  }

  /** 可中文的正确 MD5 */
  public static String chineseMd5(String inStr) {
    if (inStr == null) {
      return null;
    }
    try {
      // 拿到一个MD5转换器（如果想要SHA1参数换成”SHA1”）
      MessageDigest messageDigest = MessageDigest.getInstance("MD5");
      // 输入的字符串转换成字节数组
      byte[] inputByteArray = inStr.getBytes(StandardCharsets.UTF_8);
      // inputByteArray是输入字符串转换得到的字节数组
      messageDigest.update(inputByteArray);
      // 转换并返回结果，也是字节数组，包含16个元素
      byte[] resultByteArray = messageDigest.digest();
      // 字符数组转换成字符串返回
      return byteArrayToHex(resultByteArray);
    } catch (NoSuchAlgorithmException e) {
      return null;
    }
  }

  /** 加密解密算法 执行一次加密，两次解密 */
  public static String convertSecurity(String inStr) {
    char[] a = inStr.toCharArray();
    for (int i = 0; i < a.length; i++) {
      a[i] = (char) (a[i] ^ 't');
    }
    return new String(a);
  }

  private static String byteArrayToHex(byte[] byteArray) {
    // new一个字符数组，这个就是用来组成结果字符串的（解释一下：一个byte是八位二进制，也就是2位十六进制字符（2的8次方等于16的2次方））
    char[] resultCharArray = new char[byteArray.length * 2];
    // 遍历字节数组，通过位运算（位运算效率高），转换成字符放到字符数组中去
    int index = 0;
    for (byte b : byteArray) {
      resultCharArray[index++] = HEX_DIGITS[b >>> 4 & 0xf];
      resultCharArray[index++] = HEX_DIGITS[b & 0xf];
    }

    // 字符数组组合成字符串返回
    return new String(resultCharArray);
  }
}
