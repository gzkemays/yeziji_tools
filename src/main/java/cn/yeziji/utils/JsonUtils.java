package cn.yeziji.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * @author gzkemays
 * @date 2021/3/12 14:07
 */
public class JsonUtils {
  /**
   * 解析 json 字符串获取结果，多层时，可以用 a, b, c 来进行递归解析
   *
   * @param result
   * @param key
   * @return
   */
  public static String strJsonGetValueByKey(String result, String key) {
    JSONObject json = JSONObject.parseObject(result);
    if (key.contains(",")) {
      ArrayList<String> keys =
          new ArrayList<>(Collections.synchronizedList(Arrays.asList(key.split(","))));
      String newJson = json.getString(keys.get(0));
      keys.remove(0);
      return strJsonGetValueByKey(newJson, keys.get(0).trim());
    }
    return json.getString(key.trim());
  }

  /**
   * 判断是否为 JSON
   *
   * @param result 需要判断的字符串
   * @return false 为否，true 为是
   */
  public static boolean isJson(String result) {
    if (StringUtils.isEmpty(result)) {
      return false;
    }
    boolean isJsonArray = true;
    boolean isJsonObject = true;
    try {
      JSONObject.parseObject(result);
    } catch (Exception e) {
      isJsonObject = false;
    }
    try {
      JSONArray.parseObject(result);
    } catch (Exception e) {
      isJsonArray = false;
    }
    return isJsonArray || isJsonObject;
  }
}
