package cn.yeziji.utils;

import cn.yeziji.common.GResult;
import cn.yeziji.enums.GCommonEnums;

public class GResultUtils<T> {
  /**
   * 成功返回
   *
   * @param obj
   * @return
   */
  public static GResult OK(Object obj) {
    return GResult.init().create(GCommonEnums.SUCCESS, obj);
  }

  /**
   * 失败返回
   *
   * @param obj
   * @return
   */
  public static GResult FAIL(Object obj) {
    return GResult.init().create(GCommonEnums.FAIL, obj);
  }

  public static GResult FAIL() {
    return GResult.init().create(GCommonEnums.FAIL, null);
  }
}
