package cn.yeziji.utils;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author gzkemays
 * @date 2021/1/26 16:15
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DateFormatUtils {
  public static final String NORMAL_FORMAT = "yyyy-MM-dd HH:mm:ss";
  public static final String NORMAL2_FORMAT = "yyyy-MM-dd";
  /** 时间 String 的时间转换为英文格式 */
  public static Date toDate(String eng, String timeZone) throws ParseException {
    SimpleDateFormat dateFormat =
        new SimpleDateFormat("EEE MMM d HH:mm:ss '" + timeZone + "' yyyy", Locale.ENGLISH);
    return dateFormat.parse(eng);
  }

  /** 英文 Date 时间转基本时间 */
  public static String convertDate(String date, Integer mode, String timeZone) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    try {
      Date cdate = toDate(date, timeZone);
      if (mode == 0) {
        long time = cdate.getTime();
        return String.valueOf(time);
      } else {
        return dateFormat.format(cdate);
      }
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return null;
  }

  /** String 转换为时间 */
  public static Date strToDate(String str, String format) throws ParseException {
    SimpleDateFormat dateFormat = new SimpleDateFormat(format);
    return dateFormat.parse(str);
  }

  /** Date 转换为 Str */
  public static String dateToStr(Date date, String format) {
    SimpleDateFormat dateFormat = new SimpleDateFormat(format);
    return dateFormat.format(date);
  }
}
