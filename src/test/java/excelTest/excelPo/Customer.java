package excelTest.excelPo;

import cn.yeziji.excel.annotation.EnableExport;
import cn.yeziji.excel.annotation.EnableExportField;
import cn.yeziji.excel.annotation.ImportIndex;
import lombok.Data;

/**
 * 用户信息
 *
 * @author gzkemays
 * @date 2021/8/8 11:57
 */
@EnableExport(fileName = "用户信息")
@Data
public class Customer {
  @EnableExportField(name = "用户名")
  @ImportIndex(index = 1)
  private String username;

  @EnableExportField(name = "性别")
  @ImportIndex(index = 2)
  private String sex;

  @EnableExportField(name = "年龄")
  @ImportIndex(index = 3)
  private Integer age;
}
