import cn.yeziji.utils.ThreadPoolConfigUtils;

/**
 * @author gzkemays
 * @date 2021/9/24 16:38
 */
public class Test02 {
  public static void main(String[] args) {
    ThreadPoolConfigUtils.builder()
        .createThreadPool()
        .operation(
            e -> {
              for (int i = 0; i < 10; i++) {
                e.execute(
                    () -> {
                      System.out.println(Thread.currentThread().getName());
                        try {
                            Thread.sleep(300);
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }
                    });
              }

              return null;
            })
        .build();
  }
}
