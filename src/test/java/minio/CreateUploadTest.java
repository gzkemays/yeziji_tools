package minio;

import cn.yeziji.entity.args.MinioComposeArgs;
import cn.yeziji.utils.MinIoOperaUtils;
import io.minio.errors.*;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * @author gzkemays
 * @date 2021/9/11 0:13
 */
public class CreateUploadTest {
  public static final String ACCESS_KEY = "yezijigroup";
  public static final String SECRET_KEY = "yezijigroup6320";
  public static final String END_POINT = "http://118.31.47.234:9000";
  public static final String BUCKET_NAME = "common-group";

  public static void main(String[] args)
      throws IOException, InvalidKeyException, InvalidResponseException, InsufficientDataException,
          NoSuchAlgorithmException, ServerException, InternalException, XmlParserException,
          ErrorResponseException {
    /*    MinioClient client =
        MinioClient.builder().credentials(ACCESS_KEY, SECRET_KEY).endpoint(END_POINT).build();
    MinIoUtils minIoUtils = new MinIoUtils(ACCESS_KEY, SECRET_KEY, END_POINT);
    List<String> list =
        minIoUtils.listObjectNames(
            BUCKET_NAME, "测试 chuck/19ddd226ea85afe1218364113a625e4d_Xshell 6.zip");
    System.out.println("list = " + list);
    Map<String, String> header = new HashMap<>();
    header.put("Content-Type", "application/x-zip-compressed");
    //    header.put("Content-Type", "image/jpeg");

    Map<String, String> userMata = new HashMap<>();
    userMata.put("type", "image/jpeg");
    List<ComposeSource> composeSources = new ArrayList<>(list.size());
    for (String chuck : list) {
      ComposeSource composeSource =
          ComposeSource.builder().bucket(BUCKET_NAME).object(chuck).build();
      composeSources.add(composeSource);
    }
    client.composeObject(
        ComposeObjectArgs.builder()
            .bucket(BUCKET_NAME)
            .object("Xshell 6.zip")
            .sources(composeSources)
            .headers(header)
            .build());*/
    // 测试
    //    String uploadUrl = minIoUtils.createUploadUrl(BUCKET_NAME, "testChuck/Test.msi", 600);
    //    System.out.println(uploadUrl);
    MinIoOperaUtils minIoOperaUtils = new MinIoOperaUtils(ACCESS_KEY, SECRET_KEY, END_POINT);
    MinioComposeArgs build =
        MinioComposeArgs.builder()
            .chuckBucket(BUCKET_NAME)
            .chuckFolder("测试 chuck")
            .chuckPrefix("19ddd226ea85afe1218364113a625e4d")
            .composeBucket(BUCKET_NAME)
            .composeFolder("测试 chuck")
            .composeFileName("Xshell 6.zip")
            .contentType("application/x-zip-compressed")
            .build();
    System.out.println("build = " + build);
    minIoOperaUtils.composeChuckFile(build);
  }
}
